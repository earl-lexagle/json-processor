package com.earl.jsonprocessor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JsonProcessorApplication

fun main(args: Array<String>) {
	runApplication<JsonProcessorApplication>(*args)
}
